#!/usr/bin/env python
import random

languages = ['Go',
             'C',
             'C++',
             'Rust',
             'Python',
             'Julia']

selection = random.choice(languages)
print('You should use {}'.format(selection))
