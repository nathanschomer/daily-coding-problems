"""
Given a string of words deliminated by
spaces, reverse the words in string.

"hello world here" -> "here world hello"
"""


def flip_words(in_str: str, delim=' ') -> str:
    return delim.join(reversed(in_str.split(delim)))


if __name__ == "__main__":

    orig = "hello world here"
    sol = "here world hello"

    ret = flip_words(orig)
    assert sol == ret,\
        "Return value of {} != {}".format(ret, sol)

    print("Pass")
