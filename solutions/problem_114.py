"""
Given a string and a set of delimiters, reverse
the words in the string while maintining the relative
order of the dilimiters.
"""
import re


def interleave(words: list, delims: list) -> str:
    ret = ''
    for word, delim in zip(words[:-1], delims):
        ret += word + delim
    ret += words[-1]
    return ret


def reverse_deliminated(in_str: str) -> str:
    words = list(reversed(re.findall("\w+", in_str)))
    delims = re.findall('[\W*]', in_str)
    return interleave(words, delims)


if __name__ == '__main__':
    assert reverse_deliminated("hello/world:here") == "here/world:hello"
    # assert reverse_deliminated("hello/world:here/") == "hello//world:here"
    print("Pass")
