#!/usr/bin/env python


def nums_sum(in_list, in_num):
    """
    Given a list of numbers and a number k,
        return weather any two numbers from
        the list add up to k
    """
    in_list = in_list.copy()
    for a in in_list:
        in_list.remove(a)
        for b in in_list:
            if a+b == in_num:
                return True
    return False

if __name__ == "__main__":

    input_list = [1, 2, 3, 4, 5]
    input_num = 10

    ret = nums_sum(input_list, input_num)
    print("Two numbers {} sum to {}: {}".format(input_list,
                                                input_num, ret))
