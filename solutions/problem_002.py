"""
Problem #2 
0837-0849

Given an array of integers, return a new array such that
each element at index i of the new array
is the product of all the numbers in the original array
except the one at i
"""

def mult_array(input_array: list) -> list:
    product = 1
    for x in input_array:
        product *= x
    return product

def foo(input_array: list) -> list:

    # readable version:
    ret = []
    for val in input_array:
        tmp = input_array.copy()
        tmp.remove(val)
        ret.append(mult_array(tmp))

    # single line version:
    ret = [mult_array([x for x in input_array if x != val]) for val in input_array]

    return ret 

if __name__ == '__main__':

    ret = foo([1, 2, 3, 4, 5])
    assert ret == [120, 60, 40, 30, 24],\
        "Return value of {} is wrong".format(ret)

    ret = foo([3, 2, 1])
    assert ret == [2, 3, 6],\
        "Return value of {} is wrong".format(ret)

    print("Pass")
