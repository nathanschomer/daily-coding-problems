"""
Given an array of integers, find the first missing positive integer
in linear time and constant space. In other words, find the lowest
positive integer that does not exist in the array.
The array can contain duplicates and negative numbers as well.
"""


def find_missing(array: list) -> int:

    n = 0

    while True:
        n += 1
        if n in array:
            continue
        else:
            return n


if __name__ == "__main__":

    assert find_missing([3, 4, -1, 1]) == 2
    assert find_missing([1, 2, 0]) == 3
    print('Pass')
