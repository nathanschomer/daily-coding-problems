class Node:
    def __init__(self, val):
        self.children = []
        self.value = val

def get_paths(root_node: Node):

    # at the leaves
    if len(root_node.children) == 0:
        return [[root_node.value]]

    # above the leaves
    paths = []
    for child in root_node.children:
        for path in get_paths(child):
            paths.append([root_node.value] + path)

    return paths

if __name__ == "__main__":

    # construct tree
    root_node = Node(0)
    root_node.children = (Node(1), Node(2))
    root_node.children[1].children = (Node(3), Node(4))

    # get all paths from root to leaves
    paths = get_paths(root_node)
    print(paths)
