// Author: Nathan Schomer
// Date:   5 May 2021

#include <stdio.h>
#include <stdint.h>

void bin(unsigned n)
{
    unsigned i;
    for (i = 1 << 7; i > 0; i = i / 2)
        (n & i) ? printf("1") : printf("0");
}

uint8_t flip_bits(uint8_t in)
{
	// Given an unsigned 8-bit integer, swap its even and odd bits.
	// The 1st and 2nd bit should be swapped, the 3rd and 4th bit
	// should be swapped and so on...
	return ((in & 0xAA) >> 1) | ((in & 0x55) << 1);
}

int main(int argc, char** argv) {

	uint8_t in = 0xAA;	
	uint8_t out = flip_bits(in);

	bin(in);
	printf(" -> ");
	bin(out);
}
