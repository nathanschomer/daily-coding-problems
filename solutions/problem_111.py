"""
Problem #111

Given a word W and a string S
find all indicies in S which 
are anagrams of W.
"""


def foo(word: str, string: str) -> list:

    indicies = []
    for idx in range(len(word)-len(string)+1):
        if set(string) == set(word[idx:idx+len(string)]):
            indicies.append(idx)

    # or this horrendous one-liner:
    indices = [idx for idx in range(len(word)-len(string)+1) if set(string) == set(word[idx:idx+len(string)])]
    
    return indicies

if __name__ == "__main__":

    ret = foo("abxaba", "ab")
    assert ret == [0, 3, 4],\
        "Return value of {} is wrong!".format(ret)
    
    print("Pass")
